package com.ruoyi.erp.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 生产进度日志对象 erp_work_process_log
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public class ErpWorkProcessLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 批次号 */
    @Excel(name = "批次号")
    private String batchNo;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderNo;

    /** 生产型号 */
    @Excel(name = "生产型号")
    private String modeNo;

    /** 汇报的工序 */
    @Excel(name = "汇报的工序")
    private String curProcess;

    /** 入数 */
    @Excel(name = "入数")
    private Long inTotal;

    /** 出数 */
    @Excel(name = "出数")
    private Long outTotal;

    /** 欠交量 */
    @Excel(name = "欠交量")
    private Long oweFqcTotal;

    /** 规格-长 */
    @Excel(name = "规格-长")
    private BigDecimal specLength;

    /** 规格-宽 */
    @Excel(name = "规格-宽")
    private BigDecimal specWide;

    /** 面积 */
    @Excel(name = "面积")
    private BigDecimal area;

    /** 汇报部门 */
    @Excel(name = "汇报部门")
    private String reportDept;

    /** 汇报时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "汇报时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date reportTime;

    /** 汇报操作人员 */
    @Excel(name = "汇报操作人员")
    private String reportUser;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setBatchNo(String batchNo) 
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo() 
    {
        return batchNo;
    }
    public void setOrderNo(String orderNo) 
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo() 
    {
        return orderNo;
    }
    public void setModeNo(String modeNo) 
    {
        this.modeNo = modeNo;
    }

    public String getModeNo() 
    {
        return modeNo;
    }
    public void setCurProcess(String curProcess) 
    {
        this.curProcess = curProcess;
    }

    public String getCurProcess() 
    {
        return curProcess;
    }
    public void setInTotal(Long inTotal) 
    {
        this.inTotal = inTotal;
    }

    public Long getInTotal() 
    {
        return inTotal;
    }
    public void setOutTotal(Long outTotal) 
    {
        this.outTotal = outTotal;
    }

    public Long getOutTotal() 
    {
        return outTotal;
    }
    public void setOweFqcTotal(Long oweFqcTotal) 
    {
        this.oweFqcTotal = oweFqcTotal;
    }

    public Long getOweFqcTotal() 
    {
        return oweFqcTotal;
    }
    public void setSpecLength(BigDecimal specLength) 
    {
        this.specLength = specLength;
    }

    public BigDecimal getSpecLength() 
    {
        return specLength;
    }
    public void setSpecWide(BigDecimal specWide) 
    {
        this.specWide = specWide;
    }

    public BigDecimal getSpecWide() 
    {
        return specWide;
    }
    public void setArea(BigDecimal area) 
    {
        this.area = area;
    }

    public BigDecimal getArea() 
    {
        return area;
    }
    public void setReportDept(String reportDept) 
    {
        this.reportDept = reportDept;
    }

    public String getReportDept() 
    {
        return reportDept;
    }
    public void setReportTime(Date reportTime) 
    {
        this.reportTime = reportTime;
    }

    public Date getReportTime() 
    {
        return reportTime;
    }
    public void setReportUser(String reportUser) 
    {
        this.reportUser = reportUser;
    }

    public String getReportUser() 
    {
        return reportUser;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("batchNo", getBatchNo())
            .append("orderNo", getOrderNo())
            .append("modeNo", getModeNo())
            .append("curProcess", getCurProcess())
            .append("inTotal", getInTotal())
            .append("outTotal", getOutTotal())
            .append("oweFqcTotal", getOweFqcTotal())
            .append("specLength", getSpecLength())
            .append("specWide", getSpecWide())
            .append("area", getArea())
            .append("reportDept", getReportDept())
            .append("reportTime", getReportTime())
            .append("reportUser", getReportUser())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
