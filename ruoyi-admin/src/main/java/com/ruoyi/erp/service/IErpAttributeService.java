package com.ruoyi.erp.service;

import java.util.List;
import com.ruoyi.erp.domain.ErpAttribute;

/**
 * 拓展属性Service接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface IErpAttributeService 
{
    /**
     * 查询拓展属性
     * 
     * @param id 拓展属性ID
     * @return 拓展属性
     */
    public ErpAttribute selectErpAttributeById(String id);

    /**
     * 查询拓展属性列表
     * 
     * @param erpAttribute 拓展属性
     * @return 拓展属性集合
     */
    public List<ErpAttribute> selectErpAttributeList(ErpAttribute erpAttribute);

    /**
     * 新增拓展属性
     * 
     * @param erpAttribute 拓展属性
     * @return 结果
     */
    public int insertErpAttribute(ErpAttribute erpAttribute);

    /**
     * 修改拓展属性
     * 
     * @param erpAttribute 拓展属性
     * @return 结果
     */
    public int updateErpAttribute(ErpAttribute erpAttribute);

    /**
     * 批量删除拓展属性
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpAttributeByIds(String ids);

    /**
     * 删除拓展属性信息
     * 
     * @param id 拓展属性ID
     * @return 结果
     */
    public int deleteErpAttributeById(String id);
}
