package com.ruoyi.erp.mapper;

import java.util.List;
import com.ruoyi.erp.domain.ErpBatchInfo;

/**
 * 生产批次Mapper接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface ErpBatchInfoMapper 
{
    /**
     * 查询生产批次
     * 
     * @param id 生产批次ID
     * @return 生产批次
     */
    public ErpBatchInfo selectErpBatchInfoById(String id);

    /**
     * 查询生产批次列表
     * 
     * @param erpBatchInfo 生产批次
     * @return 生产批次集合
     */
    public List<ErpBatchInfo> selectErpBatchInfoList(ErpBatchInfo erpBatchInfo);

    /**
     * 新增生产批次
     * 
     * @param erpBatchInfo 生产批次
     * @return 结果
     */
    public int insertErpBatchInfo(ErpBatchInfo erpBatchInfo);

    /**
     * 修改生产批次
     * 
     * @param erpBatchInfo 生产批次
     * @return 结果
     */
    public int updateErpBatchInfo(ErpBatchInfo erpBatchInfo);

    /**
     * 删除生产批次
     * 
     * @param id 生产批次ID
     * @return 结果
     */
    public int deleteErpBatchInfoById(String id);

    /**
     * 批量删除生产批次
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpBatchInfoByIds(String[] ids);
}
