## 平台简介

朋友公司是PCB生产行业，一直以来是用微软的access数据库来管理处理数据，数据同步不及时，人工操作容易出错，后来找到我，希望针对于他们工厂定制开发信息化系统，
因为之前也找过好多商业公司，pcb行业的解决方案，但他们厂小，很多流程都不适应，用起来非常不合适。
于是我利用空闲时间，给他写一个web软件，本身小厂需求也比较简单，就是简单的数据管理，及生产流程控制；
直接找了开源的快速开发框架，也调研了好多java快速开发框架，最后还是觉得ruoyi非常适合；在此感谢ruoyi作者，适合二次开发；通过与朋友半天的沟通，整体数据模型结构就清晰了，
然后用powerdesinger工具生成sql脚本，直接创建表结构；通过快速平台，自动生成完代码，然后在上面二次开发定制的需求；

- 建模pdm文件：               /sql/pcb_erp.pdm
- PCB-ERP功能结构概览： /sql/PCB-ERP.xmind
- 数据库脚本：                    /sql/pcb_db_struct.sql 增加部分基础数据，可以导入直接使用；
- 用户名：admin   密码：admin123

希望有需求开发pcb行业生产管理软件的，可以二次开发；毫无保留给个人及企业免费使用。（部分敏感数据功能已剔除）

## 部署环境
购买了阿里云ECS，和RDS数据库；ecs上安装了redis和nginx；没有单独购买redis服务；
启动成功后，登录localhost 默认端口80，也可自己修改，用本项目的脚本跑

## 内置功能

1.  订单管理：页面创建订单；
2.  WIP看板： 流水线看板；
3.  批次管理：配置系统用户所属担任职务。
4.  生产进度管理：配置系统菜单，操作权限，按钮权限标识等。
5.  入库：物料入库
6.  出库
7.  库存管理
8.  售后管理
9.  生产型号管理
10. 客户管理
11. 收货地址管理
12. 拓展属性：每个产品支持自定义增加属性
13. 拓展属性值 :针对扩展属性对应的值
14. 分类型号属性关联
15. 生产型号拓展属性关联
16. 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
16. 部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
16. 岗位管理：配置系统用户所属担任职务。
16. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
16. 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
16. 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
16. 参数管理：对系统动态配置常用参数。
16. 通知公告：系统通知公告信息发布维护。
16. 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
16. 登录日志：系统登录日志记录查询包含登录异常。
16. 在线用户：当前系统中活跃用户状态监控。
16. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
16. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
16. 系统接口：根据业务代码自动生成相关的api接口文档。
16. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 缓存监控：对系统的缓存查询，删除、清空等操作。
16. 在线构建器：拖动表单元素生成相应的HTML代码。
16. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。


## 演示图
<table>
    <tr>
        <td><img src="https://gitee.com/xww520/pcb-erp/raw/dev/sql/screenshot/login.png"/></td>
        <td><img src="https://gitee.com/xww520/pcb-erp/raw/dev/sql/screenshot/6.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/xww520/pcb-erp/raw/dev/sql/screenshot/2.png"/></td>
        <td><img src="https://gitee.com/xww520/pcb-erp/raw/dev/sql/screenshot/3.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/xww520/pcb-erp/raw/dev/sql/screenshot/4.png"/></td>
        <td><img src="https://gitee.com/xww520/pcb-erp/raw/dev/sql/screenshot/5.png"/></td>
    </tr>
	<tr>
        <td><img src="https://gitee.com/xww520/pcb-erp/raw/dev/sql/screenshot/10.png"/></td>
        <td><img src="https://gitee.com/xww520/pcb-erp/raw/dev/sql/screenshot/7.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://gitee.com/xww520/pcb-erp/raw/dev/sql/screenshot/8.png"/></td>
        <td><img src="https://gitee.com/xww520/pcb-erp/raw/dev/sql/screenshot/9.png"/></td>
    </tr>
</table>


## 疑问咨询
若使用有问题，可咨询我，wx:david6698
